// console.log("Hello World")

function mySum(a1, a2) {
  sum = a1 + a2;
  console.log("Displayed sum of " +a1+ " and " +a2);
  console.log(sum);
}
mySum(5, 6);

function myDiff(s1, s2) {
  difference = s1 - s2;
  console.log("Displayed difference of " +s1+ " and " +s2);
  console.log(difference);
}
myDiff(5, 5);


function myProd(m1, m2) {
  product = m1 * m2;
  console.log("Displayed product of " +m1+ " and " +m2);
  return product;
}
myProd(5, 5);
let globalProd = product;
console.log(globalProd);

function myQuo(d1, d2) {
  quotient = d1 / d2;
  console.log("Displayed quotient of " +d1+ " and " +d2);
  return quotient;
}
myQuo(5, 5);
let globalQuo = quotient;
console.log(globalQuo);

function areaCircle(radius){
	area = 3.14159 * (radius**2);
	console.log("The result of getting the area of a circle with " +radius+ " radius:");
	return area;
}
areaCircle(5);
let circleArea = area;
console.log(circleArea);


function totalAverage(ave1, ave2, ave3, ave4){
	average = (ave1 + ave2 + ave3 + ave4) / 4;
	console.log("The average of " +ave1+ " , " +ave2+ " , " +ave3+ " and " +ave4);
	return average;
}
totalAverage(60,70,85,55);
let averageVar = average;
console.log(averageVar);

function myScore(score, total){
	percentage = (score / total)*100;
	console.log("Is " +score+"/"+total+ " a passing score?");
	isPassed = percentage > 75;
	return isPassed;
}
myScore(50, 90);
let isPassingScore = isPassed;
console.log(isPassingScore);
